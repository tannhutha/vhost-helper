# vHost Helper

Create, delete, and set ACL root directory permissions for Apache Virtualhost—no need to type lines of command line.

## Prerequisites ##

1. Install ACL if you haven't already done so.

```
apt -y install acl
```

2. Your user needs sudoer permission if it doesn't already.

```
su root
usermod -aG sudo youruser
```

## Installation ##

1. Git clone this repo
2. Apply execution permission:

```
$ chmod +x /path/to/virtualhost.sh
```

3. Copy to local bin to execute globally.

```
$ sudo cp /path/to/virtualhost.sh /usr/local/bin/virtualhost
```

## How to Use ##

### Basic Syntax ###

```
sudo virtualhost create [yourdomain.com] [/path/to/domain/root/directory/html]
sudo virtualhost delete [yourdomain.com]
sudo virtualhost setpermissions [/path/to/domain/root/directory/html]
```

### Create a New Virtualhost ###

Create a new virtualhost entry for 'my-domain.com' pointing to the root directory '/var/www/my-domain.com/html'. The root directory will be automatically created if it doesn't already exist. Default ACL permissions will be set on it.

```
sudo virtualhost create my-domain.com /var/www/my-domain.com/html
```

### Set Permissions ###

In the event you wish to reset the default ACL permissions of an exisiting root directory.

```
sudo virtualhost setpermissions /var/www/my-domain.com/html
```

### Delete a Virtualhost ###

Removes the virtualhost entry, but does not delete the root directory.

```
sudo virtualhost delete my-domain.com
```

## Default ACL Permissions ##

1. Ownership: [your-user]:www-data
2. Permission: rwxrw----,g+s
3. ACL default: o:-,u:www-data:rwx
4. ACL: u:www-data:rwx