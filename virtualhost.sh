#!/bin/bash
### Set Language
TEXTDOMAIN=virtualhost

### Set default parameters
action=$1
domain=$2
rootDir=$3
owner=$(who am i | awk '{print $1}')
apacheUser=$(ps -ef | egrep '(httpd|apache2|apache)' | grep -v root | head -n1 | awk '{print $1}')
email='webmaster@localhost'
sitesEnabled='/etc/apache2/sites-enabled/'
sitesAvailable='/etc/apache2/sites-available/'
userDir='/var/www/'
sitesAvailabledomain=$sitesAvailable$domain.conf

### don't modify from here unless you know what you are doing ####

if [ "$(whoami)" != 'root' ]; then
	echo $"You have no permission to run $0 as non-root user. Use sudo"
		exit 1;
fi

if [ "$action" != 'create' ] && [ "$action" != 'delete' ] && [ "$action" != 'setpermissions' ]
	then
		echo $"You need to prompt for action (create or delete) -- Lower-case only"
		exit 1;
fi

if [ "$action" == 'setpermissions' ] 
	then
		rootDir=$domain
		domain='not-used'
fi

while [ "$domain" == "" ]
do
	echo -e $"Please provide domain. e.g.dev,staging"
	read domain
done

if [ "$rootDir" == "" ]; then
	rootDir=${domain//./}
fi

### if root dir starts with '/', don't use /var/www as default starting point
if [[ "$rootDir" =~ ^/ ]]; then
	userDir=''
fi

rootDir=$userDir$rootDir

if [ "$action" == 'create' ]
then
	### check if domain already exists
	if [ -e $sitesAvailabledomain ]; then
		echo -e $"This domain already exists.\nPlease Try Another one"
		exit;
	fi

	### check if directory exists or not
	if ! [ -d $rootDir ]; then
		echo $"The directory $rootDir does not exist... creating it..."
		### create the directory
		mkdir -p $rootDir
		### give permission to root dir
		chmod 755 $rootDir
		### write test file in the new domain dir
		if ! echo "<?php echo phpinfo(); ?>" > $rootDir/phpinfo.php
		then
			echo $"ERROR: Not able to write in file $rootDir/phpinfo.php. Please check permissions"
			exit;
		else
			echo $"Added content to $rootDir/phpinfo.php"
		fi
		virtualhost setpermissions $rootDir
		### Update directory permissions
		#if [ "$owner" == "" ]; then
		#	iam=$(whoami)
		#	if [ "$iam" == "root" ]; then
		#		chown -R $apacheUser:$apacheUser $rootDir
		#		echo $"Permissions recursively set to $apacheUser:$apacheUser"
		#	else
		#		chown -R $iam:$iam $rootDir
		#		echo $"Permission recursively set to $iam:$iam"
		#	fi
		#else
		#	chown -R $owner:$owner $rootDir
		#	echo $"Permission recursively set to $owner:$owner"
		#fi
	else
		echo $"The directory $rootDir already exist."
	fi

	### create virtual host rules file
	if ! echo "
	<VirtualHost *:80>
		ServerAdmin $email
		ServerName $domain
		ServerAlias www.$domain
		DocumentRoot $rootDir
		<Directory />
			AllowOverride All
		</Directory>
		<Directory $rootDir>
			Options Indexes FollowSymLinks MultiViews
			AllowOverride all
			Require all granted
		</Directory>
		ErrorLog /var/log/apache2/$domain-error.log
		LogLevel error
		CustomLog /var/log/apache2/$domain-access.log combined
	</VirtualHost>" > $sitesAvailabledomain
	then
		echo -e $"There is an ERROR creating $domain file"
		exit;
	else
		echo -e $"\nNew Virtual Host Created\n"
	fi

	### Add domain in /etc/hosts
	if ! echo "127.0.0.1	$domain" >> /etc/hosts
	then
		echo $"ERROR: Not able to write in /etc/hosts"
		exit;
	else
		echo -e $"Host added to /etc/hosts file \n"
	fi

	### Add domain in /mnt/c/Windows/System32/drivers/etc/hosts (Windows Subsytem for Linux)
	if [ -e /mnt/c/Windows/System32/drivers/etc/hosts ]
	then
		if ! echo -e "\r127.0.0.1       $domain" >> /mnt/c/Windows/System32/drivers/etc/hosts
		then
			echo $"ERROR: Not able to write in /mnt/c/Windows/System32/drivers/etc/hosts (Hint: Try running Bash as administrator)"
		else
			echo -e $"Host added to /mnt/c/Windows/System32/drivers/etc/hosts file \n"
		fi
	fi

	#if [ "$owner" == "" ]; then
	#	iam=$(whoami)
	#	if [ "$iam" == "root" ]; then
	#		chown -R $apacheUser:$apacheUser $rootDir
	#	else
	#		chown -R $iam:$iam $rootDir
	#	fi
	#else
	#	chown -R $owner:$owner $rootDir
	#fi

	### enable website
	a2ensite $domain

	### restart Apache
	/etc/init.d/apache2 reload

	### show the finished message
	echo -e $"Complete! \nYou now have a new Virtual Host \nYour new host is: http://$domain \nAnd its located at $rootDir"
	exit;
fi

if [ "$action" == 'setpermissions' ]
then
	if [ "$owner" == "" ]; then
		iam=$(whoami)
		if [ "$iam" == "root" ]; then
			#chown -R $apacheUser:$apacheUser $rootDir
			#echo $"Permissions recursively set to $apacheUser:$apacheUser"
			owner=$apacheUser
		else
			#chown -R $iam:$iam $rootDir
			#echo $"Permission recursively set to $iam:$iam"
			owner=$iam
		fi
	fi
	chown $owner:www-data $rootDir
	echo $"Ownership set to $owner:www-data"
	chmod 0750 $rootDir
	echo $"Permissions set to rwxr-x---"
	chmod g+s $rootDir
	echo $"Added group flag -s-"
	setfacl -d -m o:-,u:www-data:rwx $rootDir
	setfacl -m u:www-data:rwx $rootDir
	echo $"ACL set to o:--, u:www-data:rwx"
	echo $"Displaying ACL..."
	getfacl $rootDir
	echo -e $"~~DONE~~"
	exit;
fi

if [ "$action" == 'delete' ]
then
	### check whether domain already exists
	if ! [ -e $sitesAvailabledomain ]; then
		echo -e $"This domain does not exist.\nPlease try another one"
		exit;
	else
		### Delete domain in /etc/hosts
		newhost=${domain//./\\.}
		sed -i "/$newhost/d" /etc/hosts

		### Delete domain in /mnt/c/Windows/System32/drivers/etc/hosts (Windows Subsytem for Linux)
		if [ -e /mnt/c/Windows/System32/drivers/etc/hosts ]
		then
			newhost=${domain//./\\.}
			sed -i "/$newhost/d" /mnt/c/Windows/System32/drivers/etc/hosts
		fi

		### disable website
		a2dissite $domain

		### restart Apache
		/etc/init.d/apache2 reload

		### Delete virtual host rules files
		rm $sitesAvailabledomain
	fi

	### check if directory exists or not
	if [ -d $rootDir ]; then
		echo -e $"Delete host root directory ? (y/n)"
		read deldir

		if [ "$deldir" == 'y' -o "$deldir" == 'Y' ]; then
			### Delete the directory
			rm -rf $rootDir
			echo -e $"Directory deleted"
		else
			echo -e $"Host directory conserved"
		fi
	else
		echo -e $"Host directory not found. Ignored"
	fi

	### show the finished message
	echo -e $"Complete!\nYou just removed Virtual Host $domain"
	exit 0;
fi
